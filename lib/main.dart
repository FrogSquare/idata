import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:idata/channels.dart';

import 'package:idata/routes.dart';
import 'package:idata/screens/index.dart';
import 'package:idata/screens/onboard/onboardpage.dart';
import 'package:idata/theme.dart';

import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.manual,
    overlays: [
      SystemUiOverlay.top,
      SystemUiOverlay.bottom,
    ],
  );

  StreamingSharedPreferences.instance.then((prefs) async {
    final firstInstall = prefs.getBool("FirstInstall", defaultValue: true);
    final themeMode = prefs.getInt(
      "theme_mode",
      defaultValue: ThemeMode.system.index,
    );

    final route =
        (firstInstall.getValue()) ? OnboardPage.routeName : IndexPage.routeName;

    runApp(MyApp(
      initialRoute: route,
      themeMode: themeMode,
    ));
  });
}

class MyApp extends StatelessWidget {
  final String initialRoute;
  final Preference<int> themeMode;

  const MyApp({
    Key? key,
    required this.initialRoute,
    required this.themeMode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferenceBuilder(
        preference: themeMode,
        builder: (context, mode) {
          return MaterialApp(
            title: 'iData',
            themeMode: ThemeMode.system,
            theme: lightTheme,
            darkTheme: darkTheme,
            routes: routes,
            initialRoute: initialRoute,
          );
        });
  }
}
