import 'package:flutter/material.dart';
import 'package:idata/screens/index.dart';
import 'package:idata/screens/onboard/onboardpage.dart';

final Map<String, WidgetBuilder> routes = {
  OnboardPage.routeName: (_) => const OnboardPage(),
  IndexPage.routeName: (_) => const IndexPage()
};
