import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:idata/channels.dart';
import 'package:idata/screens/index.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

const List<Map<String, String>> _pageDetails = [
  {"action": "Get Started", "banner": "images/application.png"},
  {"action": "Grant Access", "banner": "images/app_data.png"},
  {"action": "Grant", "banner": "images/usage_data.png"},
];

class OnboardPage extends StatefulWidget {
  static const routeName = "OnBoardPage";

  const OnboardPage({Key? key}) : super(key: key);

  void onAccepted(BuildContext context) {}

  @override
  State<StatefulWidget> createState() => _PageState();
}

class _PageState extends State<OnboardPage> with WidgetsBindingObserver {
  bool _loading = true;
  bool _accepted = false;
  int _stage = 0;

  late Map _status;

  late Preference<bool> _firstInstall;

  @override
  void initState() {
    StreamingSharedPreferences.instance.then((prefs) async {
      _firstInstall = prefs.getBool("FirstInstall", defaultValue: true);

      try {
        final result = await platform.invokeMethod("get_started");
        _status = result;
      } catch (e) {
        _showSnackBar("Platform Load error $e");
      }

      _stage = _guessState();
      _loading = false;
      if (mounted) setState(() {});
    });

    super.initState();
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      platform.invokeMethod("isAccessGranted").then((result) {
        if (result) {
          _status['access_granted'] = true;
          _accepted = false;

          _stage = _guessState();
          if (_stage >= _pageDetails.length) {
            _accepted = true;
            Navigator.pushReplacementNamed(context, IndexPage.routeName);
          }

          if (mounted) setState(() {});
        } else {
          setState(() => _accepted = false);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: (_loading || _stage >= _pageDetails.length)
              ? _buildLoadedWidget()
              : _buildBody(),
        ),
      ),
    );
  }

  Widget _buildLoadedWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CircularProgressIndicator(),
          SizedBox(height: 15),
          Text("Loading..."),
        ],
      ),
    );
  }

  Widget _buildBody() {
    final size = MediaQuery.of(context).size;
    final details = _pageDetails[_stage];

    return Column(
      children: [
        Expanded(
          child: Center(
            child: Image(
              image: AssetImage(details["banner"]!),
            ),
          ),
        ),
        const Expanded(child: Text("Get Started Here")),
        _buildStageIndicator(),
        const SizedBox(height: 50),
        if (!_accepted)
          ElevatedButton(
            style:
                ElevatedButton.styleFrom(minimumSize: Size(size.width / 2, 48)),
            onPressed: () {
              _onAccepted();
            },
            child: Text(details["action"] ?? "OK"),
          ),
        const SizedBox(height: 20)
      ],
    );
  }

  Widget _buildStageIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(_pageDetails.length, (index) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          height: 10,
          width: 10,
          decoration: BoxDecoration(
            color: (_stage == index)
                ? Theme.of(context).colorScheme.primary
                : Theme.of(context).colorScheme.secondary,
            borderRadius: BorderRadius.circular(10),
          ),
        );
      }),
    );
  }

  _onAccepted() async {
    setState(() => _accepted = true);

    try {
      if (_stage == 0) {
        _firstInstall.setValue(false);
      } else if (_stage == 1) {
        platform.invokeMethod("requestAccess");
        return;
        //_status['access_granted'] = result;
      } else {
        final result = await platform.invokeMethod("requestPermission");
        _status['permission_granted'] = result;
      }
    } catch (e) {
      _showSnackBar("Platform Exception: $e");
      setState(() => _accepted = false);
      return;
    }

    _accepted = false;
    _stage = _guessState();
    if (_stage >= _pageDetails.length) {
      Navigator.pushReplacementNamed(context, IndexPage.routeName);
    }

    if (mounted) setState(() {});
  }

  int _guessState() {
    if (_firstInstall.getValue() == true) {
      return 0;
    }

    final access = _status['access_granted'];
    final permission = _status['permission_granted'];

    if (access == false && permission == false) {
      return 1;
    } else if (access == true && permission == false) {
      return 2;
    } else if (access == false && permission == true) {
      return 1;
    }

    return 3;
  }

  _showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message)),
    );
  }
}
