import 'package:flutter/material.dart';
import 'package:idata/sections/section.dart';
import 'package:idata/sections/sectionslist.dart';
import 'package:idata/sections/sectiontile.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class AppSettings {
  late Preference<bool> showRealtime;
  late Preference<int> themeMode;
  late Preference<int> intervalType;

  AppSettings(StreamingSharedPreferences prefs)
      : themeMode = prefs.getInt("theme_mode", defaultValue: 0),
        intervalType = prefs.getInt("interval_type", defaultValue: 0),
        showRealtime = prefs.getBool("show_realtime", defaultValue: false);
}

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PageState();
}

class _PageState extends State<SettingsPage> {
  bool _loading = true;

  late AppSettings settings;

  @override
  void initState() {
    StreamingSharedPreferences.instance.then((value) {
      settings = AppSettings(value);
      _loading = false;
      if (mounted) setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _loading ? _buildLoadingWidget() : _buildBody();
  }

  Widget _buildLoadingWidget() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _buildBody() {
    return SectionsList(
      contentPadding: const EdgeInsets.only(
        top: 10.0,
        bottom: 10.0,
      ),
      sections: [
        Section(
          title: "Notification",
          tiles: [
            SectionTile(
              title: "Traffic Period",
              subtitle: _intervalTypeToString(settings.intervalType.getValue()),
              onPressed: (context) => _showTrafficOptions(),
            ),
            SectionTile.switchTile(
              title: "Show Network Speed",
              switchValue: settings.showRealtime.getValue(),
              onToggle: (value) {
                settings.showRealtime.setValue(value);
                if (mounted) setState(() {});
              },
            ),
          ],
        ),
        Section(
          title: "Application UI",
          tiles: [
            SectionTile(
              title: "Theme",
              subtitle: _themeModeToString(settings.themeMode.getValue()),
              onPressed: (context) => _showThemeOptions(),
            ),
          ],
        ),
      ],
    );
  }

  _showTrafficOptions() async {
    final result = await _showSelection(
      ["Day", "Week", "Month"],
      settings.intervalType.getValue(),
    );

    print(result);
  }

  _showThemeOptions() async {
    final result = await _showSelection(
      ["System", "light", "dark"],
      settings.themeMode.getValue(),
    );

    print(result);
  }

  Future<dynamic> _showSelection(List<String> list, int current) async {
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(list[index]),
              trailing: (current == index) ? const Icon(Icons.check) : null,
              onTap: () => Navigator.pop(context, index),
            );
          },
        );
      },
    );
  }

  String _themeModeToString(int mode) {
    if (mode == ThemeMode.system.index) {
      return "System";
    } else if (mode == ThemeMode.light.index) {
      return "Light";
    } else {
      return "Dark";
    }
  }

  String _intervalTypeToString(int type) {
    if (type == 0) {
      return "Day";
    } else if (type == 1) {
      return "Week";
    } else {
      return "Month";
    }
  }
}
