package com.frogsquare.idata.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.frogsquare.idata.SingletonHolder
import java.lang.IllegalArgumentException
import java.util.concurrent.locks.ReentrantReadWriteLock

private const val DB_NAME: String = "com.frogsquare.idata.storage"
private const val DB_VERSION: Int = 1

private const val TAG: String = "DatabaseHelper"

@Suppress("UNUSED")
class DatabaseHelper constructor(context: Context): SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    private val lock: ReentrantReadWriteLock = ReentrantReadWriteLock(true)

    companion object : SingletonHolder<DatabaseHelper, Context>(::DatabaseHelper)

    override fun onCreate(p0: SQLiteDatabase?) {
        Log.d(TAG, "Creating Database $DB_NAME version $DB_VERSION")
    }

    override fun onConfigure(db: SQLiteDatabase?) {
        db?.enableWriteAheadLogging()
        super.onConfigure(db)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        Log.d(TAG, "Upgrading $DB_VERSION from $oldVersion to $newVersion")

        db?.let {
            it.beginTransaction()
            try {
                //if (oldVersion < 2) {}

                if (oldVersion == DB_VERSION) {
                    it.version = oldVersion
                    it.setTransactionSuccessful()
                } else {
                    throw IllegalArgumentException("$DB_NAME Upgraded to $oldVersion but required $DB_VERSION")
                }
            } catch (e: Throwable)  {
                Log.e(TAG, "Error ${e.message} \n"+ Log.getStackTraceString(e))
            } finally {
                it.endTransaction()
            }
        }
    }
}