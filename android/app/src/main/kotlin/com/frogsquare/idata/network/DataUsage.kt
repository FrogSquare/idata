package com.frogsquare.idata.network

import android.Manifest
import android.app.AppOpsManager
import android.app.usage.NetworkStats
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Process
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.frogsquare.idata.CacheManager
import com.frogsquare.idata.Common
import com.frogsquare.idata.MainActivity
import com.frogsquare.idata.SingletonHolder
import kotlin.contracts.contract

@Suppress("UNUSED")
class DataUsage private constructor(context: Context) {

    private val _context: Context = context
    private val _helper = NetworkStatsHelper(context)

    companion object: SingletonHolder<DataUsage, Context>(::DataUsage) {
        fun isAccessGranted(context: Context): Boolean {
            return try {
                val pm: PackageManager = context.packageManager
                val appOpsManager = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager

                val mode: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    appOpsManager.unsafeCheckOpNoThrow(
                        AppOpsManager.OPSTR_GET_USAGE_STATS,
                        Process.myUid(),
                        context.packageName
                    )
                } else {
                    @Suppress("DEPRECATION")
                    appOpsManager.checkOpNoThrow(
                        AppOpsManager.OPSTR_GET_USAGE_STATS,
                        Process.myUid(),
                        context.packageName
                    )
                }

                mode == AppOpsManager.MODE_ALLOWED
            } catch (e: PackageManager.NameNotFoundException) {
                false
            }
        }

        fun isPermissionGranted(context: Context): Boolean {
            val permission = ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_PHONE_STATE
            )

            return permission == PackageManager.PERMISSION_GRANTED
        }

        fun requestAccess(context: Context) {
            val appIntent = Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS)
            appIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(appIntent)
        }

        fun requestPermission(context: Context) {
            ActivityCompat.requestPermissions(
                context as MainActivity,
                arrayOf(Manifest.permission.READ_PHONE_STATE),
                Common.PHONE_STAT_REQUEST_ID
            )
        }
    }

    fun getApplication(threshold: Int?): List<PackageInfo> {
        val ret: ArrayList<PackageInfo> = arrayListOf()
        for (app in CacheManager.getPackages(_context)) {
            val usage = _helper.getUsageFor(app.applicationInfo.uid, null)

            if (threshold != null) {
                if (usage.total > threshold) {
                    ret.add(app)
                }
            } else {
                ret.add(app)
            }
        }

        return ret
    }

}

@Suppress("UNUSED")
data class Usage(var upload: Long, var download: Long) {
    val total: Long get () {
        return upload + download
    }

    fun append(stats: NetworkStats) {
        val bucket = NetworkStats.Bucket()
        while (stats.hasNextBucket()) {
            stats.getNextBucket(bucket)

            download += bucket.rxBytes
            upload += bucket.txBytes
        }
    }

    operator fun plusAssign(other: Usage) {
        download += other.download
        upload += other.upload
    }

    operator fun plus(other: Usage): Usage {
        return Usage(upload + other.upload, download + other.download)
    }
}

