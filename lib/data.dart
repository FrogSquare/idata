import 'package:flutter/material.dart';

enum TrafficPeriod { day, week, month }

class ShieldRule {}

class NetworkUsage {
  final int uploads;
  final int downloads;
  final DateTimeRange period;

  int get total {
    return uploads + downloads;
  }

  NetworkUsage({
    required this.period,
    this.uploads = 0,
    this.downloads = 0,
  });
}

class NetworkStat {
  final DateTimeRange period;
  final List<NetworkUsage> usageList;

  NetworkStat({
    required this.period,
    this.usageList = const [],
  });
}
