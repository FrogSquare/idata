package com.frogsquare.idata.network

import android.annotation.SuppressLint
import android.app.usage.NetworkStats
import android.app.usage.NetworkStatsManager
import android.content.Context
import android.net.NetworkCapabilities
import android.os.RemoteException
import android.telephony.TelephonyManager
import android.util.Log
import com.frogsquare.idata.TimeInterval
import java.lang.Exception

private const val TAG: String = "NetworkStatus"

@Suppress("UNUSED")
class NetworkStatsHelper (appContext: Context) {

    private val context: Context = appContext
    private val manager: NetworkStatsManager = appContext.getSystemService(Context.NETWORK_STATS_SERVICE) as NetworkStatsManager

    fun getMobileUsage(interval: TimeInterval?): Usage {
        val bucket: NetworkStats.Bucket?
        try {
            bucket = manager.querySummaryForDevice(
                NetworkCapabilities.TRANSPORT_CELLULAR,
                getSubscriberId(NetworkCapabilities.TRANSPORT_CELLULAR),
                interval?.start ?: 0,
                interval?.end ?: System.currentTimeMillis()
            )
        } catch (e: RemoteException) {
            Log.d(TAG, "Exception ${e.message}")
            return Usage(0, 0)
        }

        return Usage(bucket.txBytes, bucket.rxBytes)
    }

    fun getWifiUsage(interval: TimeInterval?): Usage {
        val bucket: NetworkStats.Bucket?
        try {
            bucket = manager.querySummaryForDevice(
                NetworkCapabilities.TRANSPORT_WIFI,
                getSubscriberId(NetworkCapabilities.TRANSPORT_WIFI),
                interval?.start ?: 0,
                interval?.end ?: System.currentTimeMillis()
            )
        } catch (e: RemoteException) {
            Log.d(TAG, "Exception ${e.message}")
            return Usage(0, 0)
        }

        return Usage(bucket.txBytes, bucket.rxBytes)
    }

    fun getMobileUsageFor(uid: Int, interval: TimeInterval?): Usage {
        val stats: NetworkStats?
        try {
            stats = manager.queryDetailsForUid(
                NetworkCapabilities.TRANSPORT_CELLULAR,
                getSubscriberId(NetworkCapabilities.TRANSPORT_CELLULAR),
                interval?.start ?: 0,
                interval?.end ?: System.currentTimeMillis(),
                uid
            )
        } catch (e: RemoteException) {
            Log.d(TAG, "Exception ${e.message}")
            return Usage(0, 0)
        }

        val usage = Usage(0 ,0)
        usage.append(stats)
        stats.close()

        return usage
    }

    fun getWiFiUsageFor(uid: Int, interval: TimeInterval?): Usage {
        val stats: NetworkStats?
        try {
            stats = manager.queryDetailsForUid(
                NetworkCapabilities.TRANSPORT_WIFI,
                getSubscriberId(NetworkCapabilities.TRANSPORT_WIFI),
                interval?.start ?: 0,
                interval?.end ?: System.currentTimeMillis(),
                uid
            )
        } catch (e: RemoteException) {
            Log.d(TAG, "Exception ${e.message}")
            return Usage(0, 0)
        }

        val usage = Usage(0 ,0)
        usage.append(stats)
        stats.close()

        return usage
    }

    fun getUsage(interval: TimeInterval?): Usage {
        val mobile = getMobileUsage(interval)
        val wifi = getWifiUsage(interval)

        return mobile + wifi
    }

    fun getUsageFor(uid: Int, interval: TimeInterval?): Usage {
        val mobile = getMobileUsageFor(uid, interval)
        val wifi = getWiFiUsageFor(uid, interval)

        return mobile + wifi
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    private fun getSubscriberId(networkType: Int): String? {
        if (networkType == NetworkCapabilities.TRANSPORT_CELLULAR) {
            return try {
                val manager =
                    context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                manager.subscriberId
            } catch (e: Exception) {
                null
            }
        }

        return ""
    }
}