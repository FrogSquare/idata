import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:idata/sections/abstract.dart';

enum _SectionTileType { simpleTile, switchTile }

class SectionTile extends AbstractTile {
  final String? title;
  final Widget? titleWidget;
  final int? titleMaxLines;
  final String? subtitle;
  final Widget? subtitleWidget;
  final int? subtitleMaxLines;
  final Widget? leading;
  final Widget? trailing;
  final Function(BuildContext context)? onPressed;
  final Function(bool value)? onToggle;
  final bool? switchValue;
  final bool enabled;
  final TextStyle? titleTextStyle;
  final TextStyle? subtitleTextStyle;
  final Color? switchActiveColor;
  final _SectionTileType type;

  const SectionTile({
    Key? key,
    this.title,
    this.titleWidget,
    this.titleMaxLines,
    this.subtitle,
    this.subtitleWidget,
    this.subtitleMaxLines,
    this.leading,
    this.trailing,
    this.titleTextStyle,
    this.subtitleTextStyle,
    this.enabled = true,
    this.onPressed,
    this.switchActiveColor,
  })  : type = _SectionTileType.simpleTile,
        onToggle = null,
        switchValue = null,
        assert(titleMaxLines == null || titleMaxLines > 0),
        assert(subtitleMaxLines == null || subtitleMaxLines > 0),
        super(key: key);

  const SectionTile.switchTile({
    Key? key,
    this.title,
    this.titleWidget,
    this.titleMaxLines,
    this.subtitle,
    this.subtitleWidget,
    this.subtitleMaxLines,
    this.leading,
    this.enabled = true,
    this.trailing,
    required this.onToggle,
    required this.switchValue,
    this.titleTextStyle,
    this.subtitleTextStyle,
    this.switchActiveColor,
  })  : type = _SectionTileType.switchTile,
        onPressed = null,
        assert(titleMaxLines == null || titleMaxLines > 0),
        assert(subtitleMaxLines == null || subtitleMaxLines > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    if (type == _SectionTileType.switchTile) {
      return SwitchListTile(
        secondary: leading,
        value: switchValue!,
        activeColor:
            switchActiveColor ?? Theme.of(context).colorScheme.secondary,
        onChanged: enabled ? onToggle : null,
        title: titleWidget ??
            Text(
              title ?? '',
              style: titleTextStyle,
              maxLines: titleMaxLines,
              overflow: TextOverflow.ellipsis,
            ),
        subtitle: subtitleWidget ??
            (subtitle != null
                ? Text(
                    subtitle!,
                    style: subtitleTextStyle,
                    maxLines: subtitleMaxLines,
                    overflow: TextOverflow.ellipsis,
                  )
                : null),
      );
    } else {
      return ListTile(
        title: titleWidget ?? Text(title ?? '', style: titleTextStyle),
        leading: leading,
        trailing: trailing,
        enabled: enabled,
        subtitle: subtitleWidget ??
            (subtitle != null
                ? Text(
                    subtitle!,
                    style: subtitleTextStyle,
                    maxLines: subtitleMaxLines,
                    overflow: TextOverflow.ellipsis,
                  )
                : null),
        onTap: onTapFunction(context) as void Function()?,
      );
    }
  }

  Function? onTapFunction(BuildContext context) => onPressed != null
      ? () {
          if (onPressed != null) {
            onPressed!.call(context);
          }
        }
      : null;
}

class CustomTile extends AbstractTile {
  final Widget child;

  const CustomTile({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
