package com.frogsquare.idata.shield

import android.content.Context
import android.content.pm.PackageInfo

private const val TAG: String = "iData.Shield.Rule"

@Suppress("UNUSED", "UNUSED_PARAMETER")
class Rule (
    val uid: Int,
    val packageName: String,
    val icon: Int,
    val name: String,
    val version: String,
    val system: Boolean,
    val internet: Boolean,
    val wifiException: Boolean,
    val roamingAllowed: Boolean
    ) {

    var changed: Boolean = false
    var related: List<String> = listOf()
    var relatedUIDs: Boolean = false

    companion object {
        fun getRules(context: Context) {
        }
    }
}