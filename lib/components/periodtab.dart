import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PeriodTab extends StatefulWidget {
  final List<String> tabs;
  final Function(int)? onTabChanged;

  const PeriodTab({
    Key? key,
    required this.tabs,
    this.onTabChanged,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TabState();
}

class _TabState extends State<PeriodTab> {
  int _currentTab = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.shade900,
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: List.generate(widget.tabs.length, (index) {
          final name = widget.tabs[index];

          return TextButton(
            child: Text(
              name,
              style: TextStyle(
                color: _currentTab == index
                    ? Theme.of(context).colorScheme.secondary
                    : Theme.of(context).colorScheme.onSurface,
              ),
            ),
            onPressed: () {
              setState(() => _currentTab = index);
              if (widget.onTabChanged != null) {
                widget.onTabChanged!.call(_currentTab);
              }
            },
          );
        }),
      ),
    );
  }
}
