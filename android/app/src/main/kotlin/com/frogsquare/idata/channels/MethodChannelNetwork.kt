package com.frogsquare.idata.channels

import android.content.Context
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

@Suppress("UNUSED")
class MethodChannelNetwork (context: Context, channel: MethodChannel): MethodChannel.MethodCallHandler {

    private val _context = context
    private val _channel = channel

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            "isConnected" -> {}
            "getTotalUsage" -> {}
            "loadUsage" -> {}
            "loadAppUsage" -> {}
            else -> result.notImplemented()
        }
    }
}