import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingList extends StatelessWidget {
  final int count;

  const LoadingList({
    Key? key,
    required this.count,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(count, (index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
          child: Container(
            height: 75,
            color: Colors.grey.shade200,
          ),
        );
      }),
    );
  }
}
