package com.frogsquare.idata.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class BootReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        intent.action?.let {
            if (it == Intent.ACTION_BOOT_COMPLETED) {
                // Start
                Log.d("idata", "Boot Completed!")
            }
        }
    }
}