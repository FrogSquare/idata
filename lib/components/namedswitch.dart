import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NamedSwitch extends StatefulWidget {
  const NamedSwitch({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NamedSwitch();
}

class _NamedSwitch extends State<NamedSwitch> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        child: Stack(
          children: [
            const SizedBox(
              width: 200,
              height: 50,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.grey.shade700,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
