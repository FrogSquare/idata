package com.frogsquare.idata.channels

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.frogsquare.idata.CacheManager
import com.frogsquare.idata.Common
import com.frogsquare.idata.Interval
import com.frogsquare.idata.TimeInterval
import com.frogsquare.idata.network.DataUsage
import com.frogsquare.idata.services.LocalService
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.ExecutorService
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

const val TAG: String = "idata.channel"

@Suppress("UNUSED")
object MethodChannelHelper {
    private var executor: ExecutorService = ThreadPoolExecutor(
        0,
        1,
        30L,
        TimeUnit.SECONDS, LinkedBlockingQueue<Runnable>()
    )
    private var handler: Handler = Handler(Looper.getMainLooper())

    fun runAsync(runnable: Runnable) {
        executor.execute {
            handler.post(runnable)
        }
    }
}

@Suppress("UNUSED")
class MethodChannelPlatform constructor(context: Context, channel: MethodChannel) : MethodChannel.MethodCallHandler {

    private val _context: Context = context
    private val _channel: MethodChannel = channel
    private var _result: MethodChannel.Result? = null

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            "init" -> {
                val granted = DataUsage.isAccessGranted(_context) && DataUsage.isPermissionGranted(_context)
                if (!LocalService.mEnabled && granted) {
                    Log.d(TAG, "Starting LocalService")
                    LocalService.start(null, _context, LocalService::class.java)
                }
            }
            "get_started" -> {
                result.success(mapOf(
                    "access_granted" to DataUsage.isAccessGranted(_context),
                    "permission_granted" to DataUsage.isPermissionGranted(_context)
                ))
            }
            "isAccessGranted" -> {
                result.success(DataUsage.isAccessGranted(_context))
            }
            "isPermissionGranted" -> {
                result.success(DataUsage.isPermissionGranted(_context))
            }
            "requestAccess" -> {
                if (!DataUsage.isAccessGranted(_context)) {
                    DataUsage.requestAccess(_context)
                } else {
                    result.success(true)
                }
            }
            "requestPermission" -> {
                if (!DataUsage.isPermissionGranted(_context)) {
                    _result = result
                    DataUsage.requestPermission(_context)
                } else {
                    result.success(true)
                }
            }
            "updateNotification" -> {
                val enabled = call.argument<Boolean>("enabled")

                if (enabled == false && LocalService.mEnabled) {
                    LocalService.stop("User Requested", _context, LocalService::class.java)
                    result.success(true)
                } else {
                    val interval = call.argument<Int>("interval")
                    val realtime = call.argument<Boolean>("realtime")

                    val bundle = Bundle()
                    bundle.putBoolean("show_realtime", realtime ?: false)
                    bundle.putInt("interval_type", interval ?: 0)

                    if (enabled == true && LocalService.mEnabled) {
                        LocalService.reload(
                            "User Requested",
                            _context,
                            LocalService::class.java,
                            bundle)
                    } else if (!LocalService.mEnabled) {
                        LocalService.start(
                            "User Requested",
                            _context,
                            LocalService::class.java,
                            bundle
                        )
                    }
                }
            }
            "getApplicationList" -> {
                GlobalScope.launch {
                    val threshold = call.argument("threshold") ?: 1024
                    val period = call.argument("period") ?: 0

                    val appList = DataUsage.getInstance(_context).getApplication(threshold)
                    val ret = appList.map {
                        mapOf(
                            "name" to CacheManager.getLabel(_context, it),
                            "package_name" to it.packageName,
                            "uid" to it.applicationInfo.uid
                        )
                    }

                    MethodChannelHelper.runAsync(Runnable {
                        result.success(ret)
                    })
                }

            }
            "gotoAppInfo" -> {}
            else -> result.notImplemented()
        }
    }

    private fun intToInterval(value: Int): TimeInterval {
        return when (value) {
            0 -> Interval.today
            1 -> Interval.week
            else -> Interval.month
        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Common.PHONE_STAT_REQUEST_ID -> {
                if (permissions[0] == Manifest.permission.READ_PHONE_STATE && _result != null) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        _result!!.success(true)
                    } else {
                        _result!!.error(
                            "USER_DENIED",
                            "Permission.READ_PHONE_STATE is denied by the user.",
                            null
                        )
                    }

                    _result = null
                }
            }
        }
    }
}