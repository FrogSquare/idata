import 'package:flutter/material.dart';
import 'package:idata/sections/section.dart';
import 'package:idata/sections/sectionslist.dart';
import 'package:idata/sections/sectiontile.dart';

class ShieldPage extends StatefulWidget {
  const ShieldPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PageState();
}

class _PageState extends State<ShieldPage> {
  @override
  Widget build(BuildContext context) {
    return SectionsList(
      sections: [
        CustomSection(
          child: Container(
            color: Theme.of(context).colorScheme.secondaryVariant,
            height: 250,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    Icon(Icons.shield, size: 123),
                    Switch(value: false, onChanged: null),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text("Block New Installed Application."),
                    Switch(value: false, onChanged: null)
                  ],
                )
              ],
            ),
          ),
        ),
        CustomSection(
          child: const ListTile(
            title: Text("Firewall"),
          ),
        ),
      ],
    );
  }
}
