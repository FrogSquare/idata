package com.frogsquare.idata.channels

import android.content.Context
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class MethodChannelShield (context: Context, channel: MethodChannel) : MethodChannel.MethodCallHandler{

    private val _context: Context = context
    private val _channel: MethodChannel = channel

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            else -> result.notImplemented()
        }
    }
}