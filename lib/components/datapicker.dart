import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  const DatePicker({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WidgetState();
}

class _WidgetState extends State<DatePicker> {
  DateTime _current = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            onPressed: _moveBackward,
            icon: const Icon(Icons.arrow_left),
            splashColor: Theme.of(context).colorScheme.primary.withOpacity(0.2),
          ),
          Text(DateFormat.MMMd().format(_current)),
          IconButton(
            onPressed: _moveForward,
            icon: const Icon(Icons.arrow_right),
            splashColor: Theme.of(context).colorScheme.primary.withOpacity(0.2),
          )
        ],
      ),
    );
  }

  _moveForward() {
    final now = _current.add(const Duration(days: 1));
    setState(() => _current = now);
  }

  _moveBackward() {
    final now = _current.subtract(const Duration(days: 1));
    setState(() => _current = now);
  }
}
