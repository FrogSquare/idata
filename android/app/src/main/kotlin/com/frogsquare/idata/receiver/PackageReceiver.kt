package com.frogsquare.idata.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class PackageReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        intent.action?.let {
            if (it == Intent.ACTION_PACKAGE_ADDED) {
                // Start
                Log.d("idata", "New Package added")
            }
        }
    }
}