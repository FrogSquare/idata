package com.frogsquare.idata

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.view.WindowManager
import com.frogsquare.idata.channels.*
import com.frogsquare.idata.events.EventChannelNetworkAccess
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel
import java.lang.reflect.Method

class MainActivity: FlutterActivity() {

    private var networkHandler: MethodChannelNetwork? = null
    private var platformHandler: MethodChannelPlatform? = null
    private var shieldHandler: MethodChannelShield? = null

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        val messenger = flutterEngine.dartExecutor.binaryMessenger
        val network = MethodChannel(messenger, Common.CHANNEL_NETWORK)
        val platform = MethodChannel(messenger, Common.CHANNEL_PLATFORM)
        val shield = MethodChannel(messenger, Common.CHANNEL_SHIELD)

        networkHandler = MethodChannelNetwork(this, network)
        platformHandler = MethodChannelPlatform(this, platform)
        shieldHandler = MethodChannelShield(this, shield)

        network.setMethodCallHandler(networkHandler)
        platform.setMethodCallHandler(platformHandler)
        shield.setMethodCallHandler(shieldHandler)

        MethodChannel(messenger, Common.CHANNEL_DATABASE)
            .setMethodCallHandler(MethodChannelSQL())

        EventChannel(messenger, Common.CHANNEL_PERMISSION)
            .setStreamHandler(EventChannelNetworkAccess(this))


        if (BuildConfig.DEBUG) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }

        createNotificationChannels()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Common.PHONE_STAT_REQUEST_ID,
            Common.USAGE_ACCESS_REQUEST_ID -> {
                platformHandler?.onRequestPermissionsResult(
                    requestCode,
                    permissions,
                    grantResults
                )
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun createNotificationChannels() {
        val channel0 = NotificationChannel(
            Common.USAGE_NOTIFICATION_CHANNEL_ID,
            "Persistent",
            NotificationManager.IMPORTANCE_DEFAULT
        )

        val channel1 = NotificationChannel(
            Common.SHIELD_NOTIFICATION_CHANNEL_ID,
            "Shield",
            NotificationManager.IMPORTANCE_DEFAULT
        )

        val channel2 = NotificationChannel(
            Common.ALERT_NOTIFICATION_CHANNEL_ID,
            "Alerts",
            NotificationManager.IMPORTANCE_DEFAULT
        )

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(channel0)
        manager.createNotificationChannel(channel1)
        manager.createNotificationChannel(channel2)
    }
}
