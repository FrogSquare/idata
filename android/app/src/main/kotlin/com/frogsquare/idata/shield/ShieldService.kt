package com.frogsquare.idata.shield

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.net.VpnService
import android.os.ParcelFileDescriptor
import android.util.Log
import com.frogsquare.idata.MainActivity
import com.frogsquare.idata.services.Command
import com.frogsquare.idata.services.LocalService
import java.io.IOException


private const val TAG: String = "idata.shield"

class ShieldService: VpnService() {
    private var vpn: ParcelFileDescriptor? = null
    private var enabled: Boolean = false

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val cmd: Command = if (intent == null) {
            Command.Start
        } else {
            intent.getSerializableExtra(LocalService.EXTRA_COMMAND) as Command
        }
        val reason: String? = intent?.getStringExtra(LocalService.EXTRA_REASON)

        Log.d(TAG, "Shield onStartCommand reason, enabled, command: $reason, $enabled, $cmd")

        when (cmd) {
            Command.Start -> {
                if (enabled && vpn == null)
                   vpn = startVpn()
            }
            Command.Restart -> {
                val prev = vpn

                if (enabled) {
                    vpn = startVpn()
                }
                if (prev != null) {
                    stopVpn(prev)
                }
            }
            Command.Stop -> {
                if (vpn != null) {
                    stopVpn(vpn!!)
                    vpn = null
                }

                stopSelf()
            }
        }

        return START_STICKY
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun startVpn(): ParcelFileDescriptor? {
        val wifi = false

        val builder = Builder()
            .setSession(this.packageName)
            .addAddress("10.1.1.10", 32)
            .addAddress("fd00:1:fd00:1:fd00:1:fd00:1", 128)
            .addRoute("0.0.0.0", 0)
            .addRoute("0:0:0:0:0:0:0:0", 0)

        //for (rule in Rule.getRules()) {
        //
        //}

        val configure = Intent(this, MainActivity::class.java)
        val pi = PendingIntent.getActivity(
            this,
            0,
            configure,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        builder.setConfigureIntent(pi)

        return try {
            builder.establish()
        } catch (e: Throwable) {
            Log.e(TAG, "${e.message} \n" + Log.getStackTraceString(e))
            null
        }
    }

    private fun stopVpn(vpn: ParcelFileDescriptor) {
        try {
            vpn.close()
        } catch (ex: IOException) {
            Log.e(TAG, "${ex.message} \n" + Log.getStackTraceString(ex))
        }
    }
}