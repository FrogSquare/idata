package com.frogsquare.idata.services

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.frogsquare.idata.*
import com.frogsquare.idata.network.NetworkStatsHelper
import com.frogsquare.idata.network.TrafficUtils
import java.util.*


enum class Command {
    Start, Stop, Restart
}

private const val TAG: String = "idata.localService"

@Suppress("UNUSED")
class LocalService: Service() {
    private var timer: Timer? = null

    private var mShowRealTime: Boolean = false
    private var mIntervalType: Int = 0

    private val networkStatsHelper by lazy {
        NetworkStatsHelper(this)
    }
    private val builder by lazy {
        Notification.Builder(this, Common.USAGE_NOTIFICATION_CHANNEL_ID)
            .setVisibility(Notification.VISIBILITY_PUBLIC)
            .setOngoing(true)
            .setAutoCancel(false)
            .setContentTitle("Data Usage")
            .setContentText("Mobile: 0, WiFi: 0")
            .setShowWhen(false)
            .setOnlyAlertOnce(true)
            .setContentIntent(Utils.getPendingActivity(this))
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val cmd: Command = if (intent == null)  {
            Command.Start
        } else {
            intent.getSerializableExtra(EXTRA_COMMAND) as Command
        }
        val reason: String? = intent?.getStringExtra(EXTRA_REASON)

        Log.d(TAG, "LocalService onStartCommand reason, status, command: $reason, $mEnabled, $cmd")

        when (cmd) {
            Command.Start -> {

                loadExtras(intent)

                timer = makeTimer()
                mEnabled = timer != null

                if (mEnabled) {
                    startForeground(Common.USAGE_NOTIFICATION_ID, builder.build())
                }
            }
            Command.Restart -> {
                loadExtras(intent)

                if (timer != null) {
                    timer!!.cancel()
                    timer = null
                }

                timer = makeTimer()
                mEnabled = timer != null
            }
            Command.Stop -> {
                if (timer != null) {
                    timer!!.cancel()
                    timer = null
                }

                mEnabled = false
                stopSelf()
            }
        }

        return START_REDELIVER_INTENT
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun loadExtras(intent: Intent?) {
        val bundle = intent?.getBundleExtra(EXTRA_BUNDLE)
        bundle?.let {
            mShowRealTime = it.getBoolean("show_realtime", mShowRealTime)
            mIntervalType = it.getInt("interval_type", mIntervalType)
        }
    }

    private fun makeTimer(): Timer {
        updateNotification()

        val timer = Timer()
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                updateNotification()
            }
        }, 0, if (mShowRealTime) 500L else 30000L)

        return timer
    }

    private fun updateNotification() {
        val traffic = TrafficUtils.getSpeed()

        if (mShowRealTime) {
            val icon = Utils.createSpeedBitmapFromString(traffic.first, traffic.second)
            builder.setSmallIcon(Icon.createWithBitmap(icon))
        } else {
            builder.setSmallIcon(R.mipmap.ic_notification)
        }

        val interval: TimeInterval? = when (mIntervalType) {
            0 -> Interval.today
            1 -> Interval.week
            2 -> Interval.month
            else -> null
        }

        val wifi = networkStatsHelper.getWifiUsage(interval)
        val mobile = networkStatsHelper.getMobileUsage(interval)

        val wifiString = Utils.formatBytes(wifi.total)
        val mobileString = Utils.formatBytes(mobile.total)

        builder.setContentText("Mobile: $mobileString, WiFi: $wifiString")

        with(NotificationManagerCompat.from(this)) {
            notify(Common.USAGE_NOTIFICATION_ID, builder.build())
        }
    }

    companion object {
        var mEnabled: Boolean = false

        const val EXTRA_COMMAND = "__extra_command__"
        const val EXTRA_REASON = "__extra_reason__"
        const val EXTRA_BUNDLE = "__extra_bundle__"

        @JvmStatic
        fun start(reason: String?, context: Context, cls: Class<*>, bundle: Bundle? = null) {
            val intent = Intent(context, cls)
            intent.putExtra(EXTRA_COMMAND, Command.Start)
            intent.putExtra(EXTRA_REASON, reason)
            intent.putExtra(EXTRA_BUNDLE, bundle)

            ContextCompat.startForegroundService(context, intent)
        }

        @JvmStatic
        fun stop(reason: String?, context: Context, cls: Class<*>, bundle: Bundle?  = null) {
            val intent = Intent(context, cls)
            intent.putExtra(EXTRA_COMMAND, Command.Stop)
            intent.putExtra(EXTRA_REASON, reason)
            intent.putExtra(EXTRA_BUNDLE, bundle)

            ContextCompat.startForegroundService(context, intent)
        }

        @JvmStatic
        fun reload(reason: String?, context: Context, cls: Class<*>, bundle: Bundle?  = null) {
            val intent = Intent(context, cls)
            intent.putExtra(EXTRA_COMMAND, Command.Restart)
            intent.putExtra(EXTRA_REASON, reason)
            intent.putExtra(EXTRA_BUNDLE, bundle)

            ContextCompat.startForegroundService(context, intent)
        }
    }
}