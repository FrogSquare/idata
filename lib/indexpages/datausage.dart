import 'package:flutter/material.dart';
import 'package:idata/channels.dart';
import 'package:idata/components/datapicker.dart';
import 'package:idata/components/loadingscreen.dart';
import 'package:idata/components/periodtab.dart';
import 'package:idata/sections/abstract.dart';
import 'package:idata/sections/section.dart';
import 'package:idata/sections/sectionslist.dart';
import 'package:idata/sections/sectiontile.dart';

class DataUsagePage extends StatefulWidget {
  const DataUsagePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PageState();
}

class _PageState extends State<DataUsagePage> {
  bool _loading = true;
  List<dynamic> _appList = [];

  @override
  void initState() {
    super.initState();

    platform.invokeMethod("getApplicationList").then((value) {
      try {
        final result = value;
        _appList = result;
        _loading = false;

        if (mounted) setState(() {});
      } catch (e) {
        _showSnackBar("Error: $e");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SectionsList(
      contentPadding: const EdgeInsets.only(bottom: 10),
      sections: [
        CustomSection(
          child: const PeriodTab(
            tabs: ["Week", "Day", "Month"],
          ),
        ),
        CustomSection(child: const DatePicker()),
        CustomSection(
          child: Container(
            color: Colors.grey.shade800,
            height: 175,
          ),
        ),
        CustomSection(child: const SectionTile(title: "App List")),
        (!_loading)
            ? Section(tiles: _buildAppList())
            : CustomSection(child: const LoadingList(count: 10)),
      ],
    );
  }

  List<AbstractTile> _buildAppList() {
    print(_appList.length);
    return List.generate(_appList.length, (index) {
      final app = _appList[index];

      return SectionTile(
        title: app["name"],
        subtitle: app["package_name"],
      );
    });
  }

  _showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }
}
