package com.frogsquare.idata.network

import android.content.Context
import android.net.TrafficStats
import android.net.wifi.WifiManager
import com.frogsquare.idata.Utils

object TrafficUtils {
    fun getSpeed(): Pair<String, String> {
        val mDownloadSpeed: String
        val mUnit: String

        val mBytesPrevious = TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes()
        try {
            Thread.sleep(1000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        val mBytesCurrent = TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes()

        val mNetworkSpeed = mBytesCurrent - mBytesPrevious
        val mDownloadSpeedWithDecimals: Float

        when {
            mNetworkSpeed >= Utils.GB -> {
                mDownloadSpeedWithDecimals = mNetworkSpeed.toFloat() / Utils.GB.toFloat()
                mUnit = "GB"
            }
            mNetworkSpeed >= Utils.MB -> {
                mDownloadSpeedWithDecimals = mNetworkSpeed.toFloat() / Utils.MB.toFloat()
                mUnit = "MB"
            }
            else -> {
                mDownloadSpeedWithDecimals = mNetworkSpeed.toFloat() / Utils.KB.toFloat()
                mUnit = "KB"
            }
        }

        mDownloadSpeed = if (mUnit != "KB" && mDownloadSpeedWithDecimals < 100) {
            "%.1f".format(mDownloadSpeedWithDecimals)
        } else {
            mDownloadSpeedWithDecimals.toInt().toString()
        }

        return Pair(mDownloadSpeed, mUnit)
    }

    fun isWifiConnected(context: Context): Boolean {
        val manager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        if (manager.isWifiEnabled) {
            val wifiInfo = manager.connectionInfo
            return wifiInfo.networkId != -1
        }

        return  false
    }
}