import 'package:flutter/services.dart';

const database = MethodChannel("com.frogsquare.idata.database");
const network = MethodChannel("com.frogsquare.idata.network");
const platform = MethodChannel("com.frogsquare.idata.platform");
const shield = MethodChannel("com.frogsquare.idata.shield");

const permissionListener =
    EventChannel("com.frogsquare.idata.permission_listener");
