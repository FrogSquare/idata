import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:idata/sections/abstract.dart';

const _KPadding = EdgeInsets.symmetric(horizontal: 10.0, vertical: 3.0);

// ignore: must_be_immutable
class Section extends AbstractSection {
  final List<AbstractTile>? tiles;
  final TextStyle? titleTextStyle;
  final int? maxLines;
  final Widget? subtitle;
  final EdgeInsetsGeometry subtitlePadding;

  Section({
    Key? key,
    String? title,
    Widget? titleWidget,
    EdgeInsetsGeometry titlePadding = _KPadding,
    this.maxLines,
    this.subtitle,
    this.subtitlePadding = const EdgeInsets.all(3.0),
    this.tiles,
    this.titleTextStyle,
  })  : assert(maxLines == null || maxLines > 0),
        super(
          key: key,
          title: title,
          titleWidget: titleWidget,
          titlePadding: titlePadding,
        );

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (title != null)
          Padding(
            padding: titlePadding!,
            child: Text(
              title!,
              style: titleTextStyle ??
                  TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontWeight: FontWeight.bold,
                  ),
              maxLines: maxLines,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        if (subtitle != null)
          Padding(
            padding: subtitlePadding,
            child: subtitle,
          ),
        if (tiles != null)
          ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: tiles!.length,
            separatorBuilder: (context, index) => const Divider(height: 1),
            itemBuilder: (context, index) => tiles![index],
          ),
        if (showBottomDivider) const Divider(height: 1),
      ],
    );
  }
}

// ignore: must_be_immutable
class CustomSection extends AbstractSection {
  final Widget child;

  CustomSection({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
