import 'package:flutter/material.dart';

class AlertPage extends StatefulWidget {
  const AlertPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PageState();
}

class _PageState extends State<AlertPage> {
  @override
  Widget build(BuildContext context) {
    final brightness = Theme.of(context).brightness;

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.alarm,
            size: 64,
            color: brightness == Brightness.light
                ? Colors.black12
                : Colors.grey.shade900,
          ),
          const SizedBox(height: 10),
          Text(
            "No Alerts available.",
            style: TextStyle(
              color: brightness == Brightness.light
                  ? Colors.black12
                  : Colors.grey.shade900,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
