package com.frogsquare.idata

object Common {
    const val USAGE_NOTIFICATION_ID: Int = 0x00013
    const val USAGE_ACCESS_REQUEST_ID: Int = 0x00014
    const val PHONE_STAT_REQUEST_ID: Int = 0x00015

    const val USAGE_NOTIFICATION_CHANNEL_ID: String = "iData.TrafficUsage"
    const val ALERT_NOTIFICATION_CHANNEL_ID: String = "iData.Alert"
    const val SHIELD_NOTIFICATION_CHANNEL_ID: String = "iData.Shield"

    const val CHANNEL_DATABASE: String = "com.frogsquare.idata.database"
    const val CHANNEL_NETWORK: String = "com.frogsquare.idata.network"
    const val CHANNEL_PLATFORM: String = "com.frogsquare.idata.platform"
    const val CHANNEL_SHIELD: String = "com.frogsquare.idata.shield"

    const val CHANNEL_PERMISSION: String = "com.frogsquare.idata.permission_listener"
}