package com.frogsquare.idata

import android.Manifest
import android.content.Context
import android.content.pm.PackageInfo
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable

@Suppress("UNUSED")
class CacheManager constructor(context: Context) {

    private val _context: Context = context

    companion object : SingletonHolder<CacheManager, Context>(::CacheManager) {
        private var packages: List<PackageInfo> = listOf()
        private var appLabels: HashMap<PackageInfo, String> = hashMapOf()
        private var appIcon: HashMap<PackageInfo, ByteArray> = hashMapOf()

        fun getPackages(context: Context): List<PackageInfo> {
            if (packages.isEmpty()) {
                val pm = context.packageManager
                val permission = arrayOf(Manifest.permission.INTERNET)
                packages = pm.getPackagesHoldingPermissions(permission, 0)
            }

            return packages;
        }

        fun getLabel(context: Context, info: PackageInfo): String? {
            if (!appLabels.containsKey(info)) {
                val pm = context.packageManager
                appLabels[info] = info.applicationInfo.loadLabel(pm).toString()
            }

            return appLabels[info]
        }

        fun getIcon(context: Context, info: PackageInfo): ByteArray? {
            if (!appIcon.containsKey(info)) {
                val pm = context.packageManager
                val icon = info.applicationInfo.loadIcon(pm) as BitmapDrawable
                appIcon[info] = Utils.bitmapToByteArray(icon.bitmap)
            }

            return appIcon[info]
        }
    }

    fun load() {
        val pm = _context.packageManager
        for (app in pm.getInstalledPackages(0)) {


        }
    }
}