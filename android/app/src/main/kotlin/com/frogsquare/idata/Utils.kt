package com.frogsquare.idata

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import java.io.ByteArrayOutputStream

object Utils {
    const val GB : Long = 1073741824
    const val MB : Long = 1048576
    const val KB : Long = 1024

    @SuppressLint("UnspecifiedImmutableFlag")
    fun getPendingActivity(context: Context): PendingIntent {
        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        return PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    fun createSpeedBitmapFromString(speed: String, unit: String): Bitmap {
        val units = "$unit/s"

        val paint = Paint()
        paint.isAntiAlias = true
        paint.textSize = 55F
        paint.textAlign = Paint.Align.CENTER

        val unitPaint = Paint()
        unitPaint.isAntiAlias = true
        unitPaint.textSize = 40F
        unitPaint.textAlign = Paint.Align.CENTER

        val speedBounds = Rect()
        paint.getTextBounds(speed, 0, speed.length, speedBounds)

        val unitBounds = Rect()
        unitPaint.getTextBounds(units, 0, units.length, unitBounds)

        val width = if (speedBounds.width() > unitBounds.width()) {
            speedBounds.width()
        } else {
            unitBounds.width()
        }

        val bitmap = Bitmap.createBitmap(width + 10, 90, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawText(speed, (width / 2F + 5), 50F, paint)
        canvas.drawText(units, width / 2F, 90F, unitPaint)

        return bitmap
    }

    fun formatBytes(usage: Long): String {
        var mUsageWithDecimal: Float = 0F
        var mUnit: String = "B"

        when {
            usage >= GB -> {
                mUsageWithDecimal = usage.toFloat() / GB.toFloat()
                mUnit = "GB"
            }
            usage >= MB -> {
                mUsageWithDecimal = usage.toFloat() / MB.toFloat()
                mUnit = "MB"
            }
            usage >= KB -> {
                mUsageWithDecimal = usage.toFloat() / KB.toFloat()
                mUnit = "KB"
            }
        }

        return if (mUnit != "KB" && mUsageWithDecimal < 100) {
            "%.1f $mUnit".format(mUsageWithDecimal)
        } else {
            "${mUsageWithDecimal.toInt()} $mUnit"
        }
    }

    fun bitmapToByteArray(bmp: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val byteArray = stream.toByteArray()
        bmp.recycle()
        return byteArray
    }
}