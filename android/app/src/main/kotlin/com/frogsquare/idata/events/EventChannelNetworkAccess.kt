package com.frogsquare.idata.events

import android.Manifest
import android.app.AppOpsManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Process
import androidx.core.content.ContextCompat
import io.flutter.plugin.common.EventChannel

@Suppress("UNUSED")
class EventChannelNetworkAccess (context: Context): EventChannel.StreamHandler {

    private val _context: Context = context
    private val appOpsManager = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager

    private var events: EventChannel.EventSink? = null

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        this.events = events

        appOpsManager.startWatchingMode(
            AppOpsManager.OPSTR_GET_USAGE_STATS,
            _context.packageName,
            listener
        )
        appOpsManager.startWatchingMode(
            AppOpsManager.OPSTR_READ_PHONE_STATE,
            _context.packageName,
            listener
        )
    }

    private val listener = AppOpsManager.OnOpChangedListener {
            op, packageName ->
        if (packageName.isEmpty() || packageName.equals(_context.packageName)) {
            if (op.equals(AppOpsManager.OPSTR_GET_USAGE_STATS)) {
                events?.success(mapOf(
                    "op" to op,
                    "status" to checkUsagePermission()
                ))
            }

            if (op.equals(AppOpsManager.OPSTR_READ_PHONE_STATE)) {
                events?.success(mapOf(
                    "op" to op,
                    "status" to checkPhoneStatPermission()
                ))
            }
        }
    }

    private fun checkUsagePermission(): Boolean {
        val mode: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            appOpsManager.unsafeCheckOpNoThrow(
                AppOpsManager.OPSTR_GET_USAGE_STATS,
                Process.myUid(),
                _context.packageName
            )
        } else {
            @Suppress("DEPRECATION")
            appOpsManager.checkOpNoThrow(
                AppOpsManager.OPSTR_GET_USAGE_STATS,
                Process.myUid(),
                _context.packageName
            )
        }

        return mode == AppOpsManager.MODE_ALLOWED
    }

    private fun checkPhoneStatPermission(): Boolean {
        val permission = ContextCompat.checkSelfPermission(
            _context,
            Manifest.permission.READ_PHONE_STATE
        )

        return permission == PackageManager.PERMISSION_GRANTED
    }

    override fun onCancel(arguments: Any?) {
        appOpsManager.stopWatchingMode(listener)
    }
}