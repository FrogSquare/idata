import 'package:flutter/material.dart';
import 'package:idata/channels.dart';
import 'package:idata/indexpages/alert.dart';
import 'package:idata/indexpages/datausage.dart';
import 'package:idata/indexpages/firewall.dart';
import 'package:idata/indexpages/settings.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class IndexPage extends StatefulWidget {
  static const String routeName = "IndexPage";

  const IndexPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  static const _pageTitles = ["iData", "Shield", "Alerts", "Settings"];
  static const List<Widget> _pages = [
    DataUsagePage(),
    ShieldPage(),
    AlertPage(),
    SettingsPage(),
  ];

  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();

    StreamingSharedPreferences.instance.then((prefs) async {
      //prefs.setBool("FirstInstall", true);

      platform.invokeMethod("init").then((value) {
        permissionListener.receiveBroadcastStream().listen((event) {
          print(event);
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pageTitles[_currentIndex]),
        elevation: (_currentIndex == 0) ? 0 : null,
        backgroundColor: Theme.of(context).colorScheme.surface,
      ),
      bottomNavigationBar: _buildBottomBar(),
      body: SafeArea(
        child: _pages[_currentIndex],
      ),
    );
  }

  Widget _buildBottomBar() {
    return BottomNavigationBar(
      elevation: 0,
      currentIndex: _currentIndex,
      backgroundColor: Theme.of(context).colorScheme.surface,
      selectedItemColor: Theme.of(context).colorScheme.secondaryVariant,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      type: BottomNavigationBarType.fixed,
      items: [
        BottomNavigationBarItem(
          label: _pageTitles[0],
          icon: IconButton(
            icon: const Icon(Icons.data_usage),
            onPressed: () => setState(() => _currentIndex = 0),
          ),
        ),
        BottomNavigationBarItem(
          label: _pageTitles[1],
          icon: IconButton(
            icon: Icon(
              (_currentIndex == 1) ? Icons.shield : Icons.shield_outlined,
            ),
            onPressed: () => setState(() => _currentIndex = 1),
          ),
        ),
        BottomNavigationBarItem(
          label: _pageTitles[2],
          icon: IconButton(
            icon: const Icon(Icons.alarm),
            onPressed: () => setState(() => _currentIndex = 2),
          ),
        ),
        BottomNavigationBarItem(
          label: _pageTitles[3],
          icon: IconButton(
            icon: Icon(
              (_currentIndex == 3) ? Icons.settings : Icons.settings_outlined,
            ),
            onPressed: () => setState(() => _currentIndex = 3),
          ),
        ),
      ],
    );
  }
}
