import 'package:flutter/material.dart';

final darkTheme = ThemeData.from(
  colorScheme: const ColorScheme(
    primary: Color(0xFFBB86FC),
    primaryVariant: Color(0xFF3700B3),
    secondary: Color(0xFFC7FC86),
    secondaryVariant: Color(0xFF7CB300),
    surface: Color(0xFF212121),
    background: Color(0xFF121212),
    error: Color(0xFFCF6679),
    onPrimary: Colors.black,
    onSecondary: Colors.black,
    onSurface: Colors.white,
    onBackground: Colors.white,
    onError: Colors.black,
    brightness: Brightness.dark,
  ),
);

final lightTheme = ThemeData.from(
  colorScheme: const ColorScheme(
    primary: Color(0xFF6200EE),
    primaryVariant: Color(0xFF3700B3),
    secondary: Color(0xFF03DAC6),
    secondaryVariant: Color(0xFF018786),
    surface: Colors.white,
    background: Colors.white,
    error: Color(0xFFB00020),
    onPrimary: Colors.white,
    onSecondary: Colors.black,
    onSurface: Colors.black,
    onBackground: Colors.black,
    onError: Colors.white,
    brightness: Brightness.dark,
  ),
);
